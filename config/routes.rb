Spree::Core::Engine.routes.draw do

  # Add your extension routes here
  namespace :admin do
    resources :announcements do
        collection do
          post :update_positions
        end
    end

  end
  resources :announcements
end

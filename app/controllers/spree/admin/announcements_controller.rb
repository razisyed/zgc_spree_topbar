module Spree
  module Admin
    class AnnouncementsController < ResourceController
      respond_to :html
      def new 
        @announcement = Spree::Announcement.new
      end

      def edit
        @announcement = Spree::Announcement.find(params[:id])
      end

      def create
        @announcement = Spree::Announcement.new(announcement_params)
        if @announcement.save
         redirect_to root_path, notice: 'Announcement was successfully created.'
         else
          render action: 'new'
        end
      end

      def hide
        ids = [params[:id], *cookies.signed[:hidden_announcement_ids]]
        cookies.permanent.signed[:hidden_announcement_ids] = ids
        respond_to do |format|
          format.html { redirect_to :back }
          format.js
        end
      end

    private

      def announcement_params
          params.require(:announcement).permit(
            :message,
            :starts_at,
            :ends_at
          )
      end

    end
  end
end
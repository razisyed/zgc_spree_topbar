ZGC Spree Topbar
==================

This extension allows the admin to upload new spree store themes from backend. This extension provides an interface where admin can manage all the themes by editing them, deleting old themes and publishing theme to store for the users.

## Requirements

This extension currently supports Ruby 2.4.2, Rails 5.1 and Spree 4.2.


## Features

Some of the current functionalities are:-

1. Upload the theme
1. Preview the theme.
2. Publish the theme on spree store.
3. Download the theme.
4. Remove the uploaded theme.
5. Modify the uploaded / published theme.


## Installation

1. Add this extension to your Gemfile:

For SPREE 4.2

  ```ruby
  gem 'zgc_spree_topbar', git: 'git@bitbucket.org:razisyed/zgc_spree_topbar.git', branch: 'master'
  ```


  *Note:- Add this gem at the end of your gemfile as it has some sprocket-rails dependency and needs to be loaded after all gems are loaded.*

2. Also add the following gem above the extension:
   ```ruby
   gem 'sprockets-helpers', '~> 1.2.1'
   ```

   *Note:- This gem is dependent for the preview feature.*

3. Install the gem using Bundler:
  ```ruby
  bundle install
  ```

4. Copy & run migrations
  ```ruby
  bundle exec rails g zgc_spree_topbar:install
  ```

5. Restart your server
  ```ruby
  rails server
  ```


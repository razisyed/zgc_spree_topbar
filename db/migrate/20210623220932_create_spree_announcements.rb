class CreateSpreeAnnouncements < ActiveRecord::Migration[6.0]
  def change
    create_table :spree_announcements do |t|
		t.text     :message
		t.string   :link_url
		t.integer  :position, null: false, default: 0
		t.boolean  :published
		t.datetime :starts_at
		t.datetime :ends_at
		
		t.timestamps
    end
  end
end
